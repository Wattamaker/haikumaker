package haikumaker;

/**
 * Author: Nicholas Wanamaker
 * Description: Enum type for use with the Word class.
 */

public enum WordType {
    NOUN, ADJECTIVE, ADVERB, VERB, ARTICLE, QUANTIFIER
}
