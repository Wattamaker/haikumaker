package haikumaker;

import java.util.*;

/**
 * Author: Nicholas Wanamaker
 * Description: This class is an specialized tree structure for use with 
 * simulating epochs in the HaikuMaker genetic algorithm
 */

public class GeneticTree {
    private Node grandMother;
    private Node grandFather;
    private int generations;
    private Node bestFit;
    
    public GeneticTree() {
        // No-arg constructor for Genetic Tree
        grandMother = null;
        grandFather = null;
        generations = 0;
    }
    
    public GeneticTree(Node grandMother, Node grandFather) {
        // General constructor for GeneticTree where the 
        // grandMother and grandFather are already constructed
        this.grandMother = grandMother;
        this.grandFather = grandFather;
        bestFit = grandMother;
        generations = 1;
    }
    
    public Node getGrandMother() {
        // Accessor method for the grandMother of the GeneticTree
        return grandMother;
    }
    
    public Node getGrandFather() {
        // Accessor method for the grandFather of the GeneticTree
        return grandFather;
    }
    
    public int getGenerations() {
        // Accessor method for the number of generations 
        // that the GeneticTree has
        return generations;
    }
    
    public int getBestFitness() {
        return bestFit.getDatum().getFitness();
    }
    
    public void setGrandMother(Node grandMother) {
        // Mutator method for setting the grandMother of the GeneticTree
        this.grandMother = grandMother;
    }
    
    public void setGrandFather(Node grandFather) {
        // Mutator method for setting the grandFather of the GeneticTree
        this.grandFather = grandFather;
    }
    
    public List<Node> getGenerationChildren(int gen) {
        // Taking in a number (starting at 1), iterates through the GeneticTree
        // to get the generation specified
        boolean foundParent = false;
        int currentGen = 1;
        Node current = grandMother;
        List<Node> children = new ArrayList<>();
        children.add(grandMother);
        children.add(grandFather);
        if(gen == 0) {
            // 0 Generations would be no chromosomes at all.
            return null;
        }
        else if(gen <= generations) {
            // Works its way through the parent nodes (ones that have 
            // children instance data) in each generation until reaching the
            // correct generation
            while(currentGen < gen) {
                if(children != null) {
                    for(int i = 0; i < children.size() && !foundParent; i++){
                        if(children.get(i).getChildren() != null) {
                            foundParent = true;
                            current = children.get(i);
                        }
                    }
                    foundParent = false;
                }
                currentGen++;
                children = current.getChildren();
            }
            return children;
        }
        else {
            // Outside the generations available
            return null;
        }
    }
    
    public Node[] getNextParents() {
        // Function that will return an array of the two best fitted children f
        // rom the latest generation.
        int currentGen = 1;
        boolean foundParent = false;
        Node[] parents = new Node[]{grandMother, grandFather};
        List<Node> children = grandMother.getChildren();
        while(currentGen < generations && children != null) {
            currentGen++;
            for(int i = 0; i < children.size(); i++) {
                // Iterates through the list of that generation's children,
                // until it finds a child that has a non-null children instance
                // data. Then it has found a child.
                if(children.get(i).getChildren() != null) {
                    foundParent = true;
                    parents = new Node[]{children.get(i), 
                        children.get(i).getSpouse()};
                }
            }
            if(!foundParent) {
                // If no parent is found, it takes the children of that generation
                // and then sorts them according to their fitness values, taking
                // the top two as the parents.
                children.sort(new NodeComparator());
                parents = new Node[]{children.get(0), children.get(1)};
                for(Node parent : parents) {
                    // Checks these parents against the best fitted chromosome
                    // to see if one of them needs replaced by it, or if they
                    // need to replace it
                    if(parent.getDatum().getFitness() > 
                            bestFit.getDatum().getFitness()) {
                        bestFit = parent;
                    }
                    else if(bestFit.getDatum().getFitness() > 
                            parent.getDatum().getFitness() 
                            && !Arrays.asList(parents).contains(bestFit)) {
                        parent = bestFit;
                    }
                }
            }
            children = parents[0].getChildren();
            foundParent = false;
        }
        return parents;
    }
    
    private class NodeComparator implements Comparator<Node> {
        // Used when sorting the children of generation to get the top two of
        // the batch
        @Override
        public int compare(Node o1, Node o2) {
            if(o1.getDatum().getFitness() < o2.getDatum().getFitness()) {
                return 1;
            }
            else if(o1.getDatum().getFitness() > o2.getDatum().getFitness()) {
                return -1;
            }
            else {
                return 0;
            }
        }
    }
    
    public void addGeneration(Node[] parents, List<Chromosome> children) {
        // Adds a new generation to the end of the GeneticTree
        generations++;
        ArrayList<Node> temp = new ArrayList<>();
        for(Chromosome child : children) {
            temp.add(new Node(child, parents[0], parents[1]));
        }
        for(Node parent : parents) {
            parent.setChildren(temp);
        }
    }
    
    public void printTree() {
        // Iterates through the GeneticTree, printing off each chromosome, 
        // seperated by generation. Finishes by printing the overall best fit
        // chromosome
        List<Node> currentGen;
        for(int i = 1; i < generations; i++) {
            currentGen = getGenerationChildren(i);
            System.out.println("Generation: " + i);
            for(Node child : currentGen) {
                    child.getDatum().printChromosome();
                    System.out.println();
            }
            System.out.println("==========================");
        }
        printBest();
    }
    
    public void printBest() {
        // Prints out the bestFit chromosome over all of the generations
        System.out.println("Best fit: " + bestFit.getDatum().getFitness());
        bestFit.getDatum().printChromosome();
    }
    
    public static class Node{
        // A node class used to link the chromosomes together 
        // within the GeneticTree structure
        private Chromosome datum;
        private Node mother;
        private Node father;
        private Node spouse; // Chromosome that it is paired with for children
        private List<Node> children;        
        
        public Node(Chromosome datum) {
            // Basic constructor that only stores the chromosome in the node
            this.datum = datum;
            mother = null;
            father = null;
            children = null;
        }
        
        public Node(Chromosome datum, Node spouse) {
            // This constructor is primarily used for starting off a simulation
            // batch, where you are constructing the two grandParents of the
            // simulation.
            this.datum = datum;
            mother = null;
            father = null;
            this.spouse = spouse;
            children = null;
        }
        
        public Node(Chromosome datum, Node mother, Node father, Node spouse) {
            // General constructor for the node storing everything but children
            // instance data
            this.datum = datum;
            this.mother = mother;
            this.father = father;
            this.spouse = spouse;
            children = null;
        }
        
        public Node(Chromosome datum, Node mother, Node father) {
            // General constructor storing datum, mother, and father
            this.datum = datum;
            this.mother = mother;
            this.father = father;
            spouse = null;
            children = null;
        }
        
        public Node(Chromosome datum, Node mother, Node father, Node spouse, List<Chromosome> children) {
            // General constructor that stores everything for the node,
            // also iterates through list of chromosomes to create them into
            // nodes as well
            this.datum = datum;
            this.mother = mother;
            this.father = father;
            this.spouse = spouse;
            for(Chromosome child : children) {
                this.children.add(new Node(child, this, this.spouse));
            }
        }
        
        protected Chromosome getDatum() {
            // Accessor method for datum (Chromosome)
            return datum;
        }
        
        protected Node getMother() {
            // Accessor method for mother
            return mother;
        }
        
        protected Node getFather() {
            // Accessor method for father
            return father;
        }
        
        protected Node getSpouse() {
            // Accessor method for spouse
            return spouse;
        }
        
        protected List<Node> getChildren() {
            // Accessor method for children
            return children;
        }
        
        protected void setDatum(Chromosome datum) {
            // Mutator method for datum
            this.datum = datum;
        }
        
        protected void setMother(Node mother) {
            // Mutator method for mother
            this.mother = mother;
        }
        
        protected void setFather(Node father) {
            // Mutator method for father
            this.father = father;
        }
        
        protected void setSpouse(Node spouse) {
            // Mutator method for spouse
            this.spouse = spouse;
        }
        
        protected void setChildren(List<Node> children) {
            // Mutator method for children
            this.children = children;
        }

    }
}