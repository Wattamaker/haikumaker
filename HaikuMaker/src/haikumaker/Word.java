package haikumaker;

/**
 * Author: Nicholas Wanamaker
 * Description: This class is for storing additional information per word in 
 * order for the genetic algorithm to perform a more thorough fitness function
 * testing on each chromosome.
 */

public class Word {
    private final String text; // The string of the word
    private final boolean startConsonant; // Whether the word starts with a consonant or not
    private final boolean endConsonant; // Whether the word ends with a consonant or not
    private final WordType type; // Whether the word is a verb or not
    private final int syllables; // How many syllables are within the word
    
    public Word(String text, boolean startConsonant, boolean endConsonant, 
            WordType type, int syllables) {
        // Constructs the Word with text, startConsonant, endConsonant, isVerb,
        // and syllables provided.
        this.text = text;
        this.startConsonant = startConsonant;
        this.endConsonant = endConsonant;
        this.type = type;
        this.syllables = syllables;
    }
    
    public String getText() {
        // Accessor method for the text of the word
        return text;
    }
    
    public String getStem() {
        // Accessor method that attempts to return the base of the word.
        int textLength = text.length();
        if(textLength < 3) {
            // Most likely is already a base
            return text;
        }
        // List of potential endings for the word
        String[] endings = new String[]{"ing", "es", "s", "e", "ed"};
        for(String ending : endings) {
            // Checks the text of the word against each ending
            if(text.substring(textLength - ending.length(), textLength).equals(ending)) {
                return text.substring(0, textLength-ending.length());
            }
        }
        // If none of the endings match, return the whole text
        return text;
    }
    
    public boolean isStartConsonant() {
        // Accessor method for seeing if the word starts with a consonant
        return startConsonant;
    }
    
    public boolean isEndConsonant() {
        // Accessor method for seeing if the word ends with a consonant
        return endConsonant;
    }
    
    public WordType getType() {
        // Accessor method for seeing if the word is a verb or not
        return type;
    }
    
    public int getSyllables() {
        // Accessor method for seeing how many sysllables the word has
        // Important to know for a haiku
        return syllables;
    }
    
    public Word createChild() {
        // For use when creating children of a chromosome, so that the same
        // object reference is not being passed around.
        return new Word(this.text, this.startConsonant, 
                this.endConsonant, this.type, this.syllables);
    }
    
}