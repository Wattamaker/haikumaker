package haikumaker;

import java.util.*;

/**
 * Author: Nicholas Wanamaker
 * Description: This class stores the actual chromosome for the genetic
 * chromosome to edit and compare.
 */

public class Chromosome {
    private Phrase[] phrases = new Phrase[3];
    private boolean isValid;
    private int fitnessValue;
    
    public Chromosome(Phrase phrase1, Phrase phrase2, Phrase phrase3) {
        // Constructs the Chromosome and puts all three phrases into an array
        this.phrases = new Phrase[]{phrase1, phrase2, phrase3};
        updateValidityAndFitness();
    }
    
    public Chromosome(Phrase[] phrases) {
        // Constructs the Chromosome with all of the phrases already in an array
        this.phrases = phrases;
        updateValidityAndFitness();
    }
    
    public Chromosome(ArrayList<Word> wordBank) {
        // Constructs the Chromosome by making three new phrases based on the 
        // words available from the wordBank, that has been passed to the
        // constructor
        this.phrases = new Phrase[]{new Phrase(wordBank, 5), 
            new Phrase(wordBank, 7), new Phrase(wordBank, 5)};
        updateValidityAndFitness();
    }
    
    public Phrase getPhrase(int index) {
        // Accessor method for an individual phrase
        return phrases[index];
    }
    
    public Phrase[] getAllPhrases() {
        // Accessor method for all of the phrases that make up the chromosome
        return phrases;
    }
    
    public Chromosome createChild() {
        // Used to create a new chromosome object from the current chromosome
        // (Essentially a copy of each part)
        return new Chromosome(new Phrase[]{phrases[0].createChild(), 
            phrases[1].createChild(), phrases[2].createChild()});
    }
    
    public void setPhrase(int index, Phrase phrase) {
        // Mutator method for changing one of the phrases
        phrases[index] = phrase;
        isValid = checkValidity();
    }
    
    public void setAllPhrases(Phrase[] phrases) {
        // Mutator method for changing all of the phrases
        this.phrases=phrases;
        isValid = checkValidity();
    }
    
    public boolean getValidity() {
        // Returns if the chromosome is a valid one
        return isValid;
    }
    
    public int getFitness() {
        // Accessor method for fitnessValue data member
        return fitnessValue;
    }
    
    public ArrayList<Chromosome> mate(Chromosome parent, int numChildren) {
        // Returns the amount of children specified, as long as the number
        // is evenly divisible by two 
        ArrayList<Chromosome> created = new ArrayList<>();
        // numChildren must be divided by two, each function returns two children
        // as a result of crossover requiring two children returned.
        for(int i = 0; i < numChildren/2; i++) {
            switch((int)(Math.random() * 3)) {
                case 0:
                    for(Chromosome child : crossover(parent)) {
                        created.add(child);
                    }
                    break;
                case 1:
                    for(Chromosome child : inversion(parent)) {
                        created.add(child);
                    }
                    break;
                case 2:
                    for(Chromosome child : mutation(parent)) {
                        created.add(child);
                    }
                    break;
            }
        }
        return created;
    }
    
    private boolean checkValidity() {
        // Checks to make sure each phrase has the correct amount of syllables
        return (phrases[0].getSyllables() == 5 
                && phrases[1].getSyllables() == 7 
                && phrases[2].getSyllables() ==5);
    }
    
    private void checkFitness() {
        // Try to avoid duplicate words accross phrases (should be addressed)
        // For flow, check consonant to consonant vs. vowel to vowel
        // If there are articles, must be attached to noun (should be addressed)
        // Avoid multiple verbs in a statement (should be addressed)
        ArrayList<String> usedWords = new ArrayList<>();
        int total = 0;
        int value;
        int verbCount;
        int nounCount;
        Word current; // Used to see word placement
        Word previous = null; // Used to see word placement
        Word next = null; // Used to see word placement
        String stem;
        for(Phrase phrase : phrases) {
            verbCount = 0;
            nounCount = 0;
            value = 0; // Fitness Points for the current Phrase
            // Trying to encourage not using only one syllable words
            value += (20 * (phrase.getSyllables() 
                    / phrase.getSize())) - 20;
            // Iterating through the phrase initially getting a noun and verb count
            // Also check for word duplicates
            for(Word word : phrase) {
                stem = word.getStem();
                if(word.getType() == WordType.VERB) {
                    verbCount++;
                }
                else if(word.getType() == WordType.NOUN) {
                    nounCount++;
                }
                if(usedWords.contains(stem)) {
                    // Heavily discouraging duplicate words in the haiku
                    value -= 8;
                }
                else {
                    usedWords.add(stem);
                }
            }
            if(verbCount > 1) {
                // Discouraging having many verbs per phrase
                value -= (verbCount - 1) * 2;
            }
            else if(verbCount == 0) {
                // Discouraging having no verbs per phrase
                value--;
            }
            else {
                // Encouraging having a verb per phrase
                value++;
            }
            if(nounCount > 1) {
                // Really discouraging having many nouns per phrase
                value -= (nounCount - 1) * 3;
            }
            else if(nounCount == 0) {
                // Discouraging having no nouns per phrase
                value--;
            }
            else {
                // Encouraging having a noun per phrase
                value++;
            }
            for(int i = 0; i < phrase.getSize(); i++) {
                current = phrase.getWord(i);
                if(i >= 1) {
                    previous = phrase.getWord(i - 1);
                }
                if(i < phrase.getSize() - 1) {
                    next = phrase.getWord(i + 1);
                }
                if(null != current.getType()) switch (current.getType()) {
                    case VERB:
                        if(previous != null
                                && previous.getType() == WordType.NOUN) {
                            // Encouraging having noun then verb structure
                            value++;
                        }   
                        break;
                    case NOUN:
                        if(previous != null) {
                            if(null != previous.getType()) switch (previous.getType()) {
                                case ARTICLE:
                                    // Encouraging having article then noun structure
                                    value += 2;
                                    break;
                                case ADJECTIVE:
                                    // Really encouraging having adjective then noun structure
                                    value += 5;
                                    break;
                                case QUANTIFIER:
                                    // Encouraging having quantifier then noun structure
                                    value += 3;
                                    break;
                                default:
                                    break;
                            }
                        }  
                        break;
                    case ADVERB:
                        if(verbCount >= 1) {
                            // Encouraging adverb with verb usage
                            value++;
                            if(previous != null
                                    && previous.getType() == WordType.VERB) {
                                // Encouraging having verb then adverb structure
                                value++;
                            }
                        }
                        else {
                            // Discouraging adverbs without verb usage
                            value -= 2;
                        }   
                        break;
                    case ADJECTIVE:
                        if(nounCount >= 1) {
                            // Encouraging adjective with noun usage
                            value++;
                            if(previous != null) {
                                if(previous.getType() == WordType.ARTICLE) {
                                    // Encouraging article then adjective usage
                                    value++;
                                }
                                else if(previous.getType() == WordType.QUANTIFIER) {
                                    //Encouraging quantifier then adjective usage
                                    value++;
                                }
                            }
                            if(next != null) {
                                if(next.getType() == WordType.NOUN) {
                                    // Encouraging adjective then noun usage
                                    value++;
                                }
                            }
                            else {
                                // Discouraging ending with an adjective
                                value -= 10;
                            }
                        }
                        else {
                            // Discouraging adjectives without noun usage
                            value -= 7;
                        }   
                        break;
                    case QUANTIFIER:
                        if(nounCount >= 1) {
                            // Encouraging quantifier with noun usage
                            value++;
                            if(previous != null) {
                                if(previous.getType() == WordType.ARTICLE) {
                                    // Encouraging article then quantifier usage
                                    value++;
                                }
                                else if(previous.getType() == WordType.ADJECTIVE) {
                                    // Discouraging adjective then quantifier usage
                                    value -= 3;
                                }
                            }
                            if(next != null) {
                                if(next.getType() == WordType.NOUN) {
                                    // Encouraging quantifier then noun usage
                                    value++;
                                }
                            }
                            else {
                                // Discouraging ending with a quantifier
                                value -= 8;
                            }
                        }
                        else {
                            // Discouraging quantifiers without noun usage
                            value -= 2;
                        }   break;
                    case ARTICLE:
                        if(nounCount >= 1) {
                            // Encouraging article with noun usage
                            value++;
                            if(next != null) {
                                if(next.getType() == WordType.NOUN) {
                                    // Encouraging article then noun usage
                                    value++;
                                }
                            }
                            else {
                                // Discouraging ending with an article
                                value -= 10;
                            }
                        }
                        else {
                            // Discouraging article without noun usage
                            value -= 2;
                        }   
                        break;
                    default:
                        break;
                }
            }
            total += value;
        }
        fitnessValue = total;
    }
    
    private void updateValidityAndFitness() {
        // This method gets called every time a chromosome is altered or
        // created
        isValid = checkValidity();
        if(isValid) {
            checkFitness();
        }
        else {
            fitnessValue = 0;
        }
    }
    
    public void printChromosome() {
        // Function for printing out the chromosome to be 
        // called from GeneticTree
        System.out.println(phrases[0].toString());
        System.out.println(phrases[1].toString());
        System.out.println(phrases[2].toString());
    }
    
    private Chromosome[] crossover(Chromosome parent) {
        // Returns two children in an array that have mixed phrases between the
        // two parents (this and one passed in). Each phrase has a 50/50 chance
        Random generator = new Random();
        Phrase[] firstChild = new Phrase[3];
        Phrase[] secondChild = new Phrase[3];
        if(generator.nextInt(10) < 5) {
            firstChild[0] = getPhrase(0).createChild();
            secondChild[0] = parent.getPhrase(0).createChild();
        }
        else {
            firstChild[0] = parent.getPhrase(0).createChild();
            secondChild[0] = getPhrase(0).createChild();
        }
        if(generator.nextInt(10) < 5) {
            firstChild[1] = getPhrase(1).createChild();
            secondChild[1] = parent.getPhrase(1).createChild();
        }
        else {
            firstChild[1] = parent.getPhrase(1).createChild();
            secondChild[1] = getPhrase(1).createChild();
        }
        if(generator.nextInt(10) < 5) {
            firstChild[2] = getPhrase(2).createChild();
            secondChild[2] = parent.getPhrase(2).createChild();
        }
        else {
            firstChild[2] = parent.getPhrase(2).createChild();
            secondChild[2] = getPhrase(2).createChild();
        }
        return new Chromosome[]{new Chromosome(firstChild), 
            new Chromosome(secondChild)};
    }
    
    private Chromosome[] inversion(Chromosome parent) {
        // Has three sub-operators that are described in their case
        Random generator = new Random();
        Phrase[] firstChild = new Phrase[3];
        Phrase[] secondChild = new Phrase[3];
        // Using switch cases so that the random integer doesn't need stored
        // for comparison
        switch(generator.nextInt(3)) {
            case 0: 
                // Reverse the order of the phrases for a child per parent
                switch(generator.nextInt(3)) {
                    case 0:
                        firstChild[0] = this.getPhrase(0).createReverseChild();
                        firstChild[1] = this.getPhrase(1).createChild();
                        firstChild[2] = this.getPhrase(2).createChild();
                        break;
                    case 1: 
                        firstChild[0] = this.getPhrase(0).createChild();
                        firstChild[1] = this.getPhrase(1).createReverseChild();
                        firstChild[2] = this.getPhrase(2).createChild();
                        break;
                    case 2: 
                        firstChild[0] = this.getPhrase(0).createChild();
                        firstChild[1] = this.getPhrase(1).createChild();
                        firstChild[2] = this.getPhrase(2).createReverseChild();
                        break;
                }
                switch(generator.nextInt(3)) {
                    case 0: 
                        secondChild[0] = parent.getPhrase(0).createReverseChild();
                        secondChild[1] = parent.getPhrase(1).createChild();
                        secondChild[2] = parent.getPhrase(2).createChild();
                        break;
                    case 1: 
                        secondChild[0] = parent.getPhrase(0).createChild();
                        secondChild[1] = parent.getPhrase(1).createReverseChild();
                        secondChild[2] = parent.getPhrase(2).createChild();
                        break;
                    case 2: 
                        secondChild[0] = parent.getPhrase(0).createChild();
                        secondChild[1] = parent.getPhrase(1).createChild();
                        secondChild[2] = parent.getPhrase(2).createReverseChild();
                        break;
                }
                break;
            case 1:
                // Flips the position of two words within a phrase for a child
                // per parent
                switch(generator.nextInt(3)) {
                    case 0: 
                        firstChild[0] = this.getPhrase(0).createFlipChild();
                        firstChild[1] = this.getPhrase(1).createChild();
                        firstChild[2] = this.getPhrase(2).createChild();
                        break;
                    case 1: 
                        firstChild[0] = this.getPhrase(0).createChild();
                        firstChild[1] = this.getPhrase(1).createFlipChild();
                        firstChild[2] = this.getPhrase(2).createChild();
                        break;
                    case 2: 
                        firstChild[0] = this.getPhrase(0).createChild();
                        firstChild[1] = this.getPhrase(1).createChild();
                        firstChild[2] = this.getPhrase(2).createFlipChild();
                        break;
                }
                switch(generator.nextInt(3)) {
                    case 0: 
                        secondChild[0] = parent.getPhrase(0).createFlipChild();
                        secondChild[1] = parent.getPhrase(1).createChild();
                        secondChild[2] = parent.getPhrase(2).createChild();
                        break;
                    case 1: 
                        secondChild[0] = parent.getPhrase(0).createChild();
                        secondChild[1] = parent.getPhrase(1).createFlipChild();
                        secondChild[2] = parent.getPhrase(2).createChild();
                        break;
                    case 2: 
                        secondChild[0] = parent.getPhrase(0).createChild();
                        secondChild[1] = parent.getPhrase(1).createChild();
                        secondChild[2] = parent.getPhrase(2).createFlipChild();
                        break;
                }
                break;
            case 2:
                // Random ordering of the words within a phrase for a child
                // per parent
                switch(generator.nextInt(3)) {
                    case 0:
                        firstChild[0] = this.getPhrase(0).createRandomChild();
                        firstChild[1] = this.getPhrase(1).createChild();
                        firstChild[2] = this.getPhrase(2).createChild();
                        break;
                    case 1: 
                        firstChild[0] = this.getPhrase(0).createChild();
                        firstChild[1] = this.getPhrase(1).createRandomChild();
                        firstChild[2] = this.getPhrase(2).createChild();
                        break;
                    case 2: 
                        firstChild[0] = this.getPhrase(0).createChild();
                        firstChild[1] = this.getPhrase(1).createChild();
                        firstChild[2] = this.getPhrase(2).createRandomChild();
                        break;
                }
                switch(generator.nextInt(3)) {
                    case 0:  
                        secondChild[0] = parent.getPhrase(0).createRandomChild();
                        secondChild[1] = parent.getPhrase(1).createChild();
                        secondChild[2] = parent.getPhrase(2).createChild();
                        break;
                    case 1:  
                        secondChild[0] = parent.getPhrase(0).createChild();
                        secondChild[1] = parent.getPhrase(1).createRandomChild();
                        secondChild[2] = parent.getPhrase(2).createChild();
                        break;
                    case 2: 
                        secondChild[0] = parent.getPhrase(0).createChild();
                        secondChild[1] = parent.getPhrase(1).createChild();
                        secondChild[2] = parent.getPhrase(2).createRandomChild();
                        break;
                }
                break;
        }
        return new Chromosome[]{new Chromosome(firstChild), 
            new Chromosome(secondChild)};
    }
    
    private Chromosome[] mutation(Chromosome parent) {
        // Replaces a random word with another word or words that are able
        // to fulfill the amount of syllables needed
        Random generator = new Random();
        Phrase[] firstChild = new Phrase[3];
        Phrase[] secondChild = new Phrase[3];
        switch(generator.nextInt(3)) {
            case 0:
                firstChild[0] = this.getPhrase(0).createMutateChild();
                firstChild[1] = this.getPhrase(1).createChild();
                firstChild[2] = this.getPhrase(2).createChild();
                break;
            case 1: 
                firstChild[0] = this.getPhrase(0).createChild();
                firstChild[1] = this.getPhrase(1).createMutateChild();
                firstChild[2] = this.getPhrase(2).createChild();
                break;
            case 2: 
                firstChild[0] = this.getPhrase(0).createChild();
                firstChild[1] = this.getPhrase(1).createChild();
                firstChild[2] = this.getPhrase(2).createMutateChild();
                break;
        }
        switch(generator.nextInt(3)) {
            case 0:  
                secondChild[0] = parent.getPhrase(0).createMutateChild();
                secondChild[1] = parent.getPhrase(1).createChild();
                secondChild[2] = parent.getPhrase(2).createChild();
                break;
            case 1:  
                secondChild[0] = parent.getPhrase(0).createChild();
                secondChild[1] = parent.getPhrase(1).createMutateChild();
                secondChild[2] = parent.getPhrase(2).createChild();
                break;
            case 2: 
                secondChild[0] = parent.getPhrase(0).createChild();
                secondChild[1] = parent.getPhrase(1).createChild();
                secondChild[2] = parent.getPhrase(2).createMutateChild();
                break;
        }
        return new Chromosome[]{new Chromosome(firstChild), 
            new Chromosome(secondChild)};
    }
    
}
