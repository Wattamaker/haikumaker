package haikumaker;

import java.util.*;
import java.io.*;

/**
 * Author: Nicholas Wanamaker
 * Description: Reads through the file of words and creates a bank of words to 
 * select from.
 */

public class WordReader {
    public static ArrayList<Word> readWords(String fileName) {
        // Opening up the file that contains all of the words
        File fromFile = new File(fileName);
        // Creating an ArrayList<Word> to store all of the words to be created
        ArrayList<Word> wordBank = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fromFile));
            String text; // Declaring the text that will be assigned during loop
            while((text = br.readLine()) != null) {
                String[] splitLine = text.split(",");
                wordBank.add(new Word(splitLine[0], 
                        Boolean.parseBoolean(splitLine[1]),
                        Boolean.parseBoolean(splitLine[2]),
                        WordType.valueOf(splitLine[3].toUpperCase()), 
                        Integer.parseInt(splitLine[4])));
            }
        }
        catch(IOException | NumberFormatException e) {
            // Catching for file reading errors, and variable type parse errors
            System.out.println(e);
            return null;
        }
        Collections.shuffle(wordBank); // So the words are not grouped by how I entered them
        return wordBank;
    }
}