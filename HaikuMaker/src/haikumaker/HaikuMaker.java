package haikumaker;

import haikumaker.GeneticTree.Node;
import java.util.*;

/**
 * Author: Nicholas Wanamaker
 * Description: This program is an implementation of a genetic algorithm that 
 * attempts to create haikus using a fitness function and passed-in word bank.
 */

public class HaikuMaker {
    public static void main(String args[]) {
        // Loads in the wordBank from CSV file, then passes to simulation
        ArrayList<Word> wordBank = WordReader.readWords("assets/wordbank.csv");
//        GeneticTree simulation = simulate(10000, 8, wordBank);
        GeneticTree simulation = achieveFitness(90, 8, wordBank);
        simulation.printBest();
    }
    
    public static GeneticTree simulate(int epochs, int numChildren, ArrayList<Word> wordBank) {
        // Passed in the amount of epochs for the Genetic Algorithm to run through
        // and the wordBank of words that the phrases can be construced from
        Node grandMother = new Node(new Chromosome(wordBank));
        Node grandFather = new Node(new Chromosome(wordBank));
        GeneticTree pool = new GeneticTree(grandMother, grandFather);
        ArrayList<Chromosome> created;
        Node[] currentParents;
        for(int i = 0; i < epochs; i++) {
            currentParents = pool.getNextParents();
            created = currentParents[0].getDatum()
                    .mate(currentParents[1].getDatum(), numChildren);
            pool.addGeneration(currentParents, created);
        }
        return pool;
    }
    
    public static GeneticTree achieveFitness(int fitnessLevel, int numChildren, ArrayList<Word> wordBank) {
        // Passed in a level of fitness, the genetic algorithm works until it 
        // achieves that level of fitness
        int genCount = 0;
        int poolCount = 0;
        Node grandMother = new Node(new Chromosome(wordBank));
        Node grandFather = new Node(new Chromosome(wordBank));
        GeneticTree pool = new GeneticTree(grandMother, grandFather);
        ArrayList<Chromosome> created;
        Node[] currentParents;
        while(pool.getBestFitness() < fitnessLevel) {
            genCount++;
            if(genCount > 1000 ) {
//                    && pool.getBestFitness() < fitnessLevel - (fitnessLevel / 4) 
//                    || genCount > 7000) {
//                System.out.println("PoolCount: " + poolCount++ + " - BestFit: " + pool.getBestFitness());
                pool = new GeneticTree(new Node(new Chromosome(wordBank)), 
                        new Node(new Chromosome(wordBank)));
                genCount = 0;
            }
            currentParents = pool.getNextParents();
            created = currentParents[0].getDatum()
                    .mate(currentParents[1].getDatum(), numChildren);
            pool.addGeneration(currentParents, created);
        }
        
        return pool;
    }
}