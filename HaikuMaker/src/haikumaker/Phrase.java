package haikumaker;

import java.util.*;

/**
 * Author: Nicholas Wanamaker
 * Description: This class is for storing the Words that make up one line of the
 * haiku.
 */

public class Phrase implements Iterable<Word>{
    private ArrayList<Word> words;
    private ArrayList<Word> wordBank;
    private int syllableCount = 0;
    
    public Phrase(ArrayList<Word> wordBank, int syllableMax) {
        // Constructs a Phrase, randomly selecting words from the wordbank being
        // passed in. The syllableMax value determines how many syllables the
        // phrase needs to hit (allows differentiation from a 5 or 7 syllable
        // phrase)
        Word temp;
        words = new ArrayList<>();
        this.wordBank = wordBank;
        while(syllableCount < syllableMax) {
            temp = wordBank.get((int)(Math.random() * wordBank.size()));
            if(temp.getSyllables() + syllableCount <= syllableMax) {
                words.add(temp);
                syllableCount += temp.getSyllables();
            }
        }
    }
    
    public Phrase(ArrayList<Word> words, ArrayList<Word> wordBank) {
        // Constructor for phrase where the words have already been assigned,
        // used mainly for the genetic operator children phrases
        this.words = words;
        this.wordBank = wordBank;
        updateSyllableCount();
    }
    
    public void setWords(ArrayList<Word> words) {
        // Used to set the words in the phrase to something else entirely
        this.words = words;
    }
    
    public Word getWord(int index) {
        // Accessor method for an individual word within words
        return words.get(index);

    }
    
    public void setWord(int index, Word word) {
        // Mutator method for an individual word within words
        words.remove(index);
        words.add(index, word);
        updateSyllableCount();
    }
    
    public boolean hasVerb() {
        // Checks to see if the phrase has a verb
        for(Word a: words) {
            if(a.getType() == WordType.VERB) {
                return true;
            }
        }
        return false; // There is no verb in the phrase
    }
    
    public Word getVerb() {
        // Returns the verb in the phrase
        for(Word a: words) {
            if(a.getType() == WordType.VERB) {
                return a;
            }
        }
        return null; // There is no verb in the phrase
    }
    
    public int getSize() {
        // Accessor method for the number of words in the phrase
        return words.size();
    }
    
    public int getSyllables() {
        return syllableCount;
    }
    
    private void updateSyllableCount() {
        // Used when a word is being changed or remove to update the syllables
        syllableCount = 0;
        for(Word a: words) {
            syllableCount += a.getSyllables();
        }
    }
    
    public Phrase createChild() {
        // Used when creating the new chromosome 
        ArrayList<Word> tempList = new ArrayList<>();
        for(Word word: words) {
            tempList.add(word);
        }
        return new Phrase(tempList, wordBank);
    }
    
    public Phrase createReverseChild() {
        // Used when create a new chromosome using a version of the inversion
        // genetic operator, returns a new phrase where the words have been
        // placed in reverse order
        ArrayList<Word> tempList = new ArrayList<>();
        for(int i = words.size() - 1; i >= 0; i--) {
            tempList.add(words.get(i));
        }
        return new Phrase(tempList, wordBank);
    }
    
    public Phrase createFlipChild() {
        // Used when creating a new chromosome using a version of the inversion
        // genetic operator, returns a new phrase where two words have switched
        // positions
        ArrayList<Word> tempList = new ArrayList<>();
        Word temp1;
        Word temp2;
        for(Word word: words) {
            tempList.add(word);
        }
        int firstNum = (int)(Math.random() * tempList.size());
        int secondNum;
        while((secondNum = (int)(Math.random() * tempList.size())) != firstNum) {
            temp1 = tempList.get(firstNum);
            temp2 = tempList.get(secondNum);
            tempList.set(firstNum, temp2);
            tempList.set(secondNum, temp1);
        }
        return new Phrase(tempList, wordBank);
    }
    
    public Phrase createRandomChild() {
        // Used when creating a new chromosome using a version of the inversion
        // genetic operator, returns a new phrase where the words have 
        // been randomly rearranged
        ArrayList<Word> tempList = new ArrayList<>();
        ArrayList<Word> fromList = new ArrayList<>();
        for(Word word: words) {
            fromList.add(word);
        }
        int temp;
        while(fromList.size() > 0) {
            temp = (int)(Math.random() * fromList.size());
            tempList.add(fromList.get(temp));
            fromList.remove(temp);
        }
        return new Phrase(tempList, wordBank);
    }
    
    public Phrase createMutateChild() {
        // Used when creating a new chromosome using the mutation genetic 
        // operator, returns a new phrase where a word has been replaced with a
        // word or words that are suitable for fulfilling the gap in syllables
        // the one being replaced had used
        ArrayList<Word> tempList = new ArrayList<>();
        for(Word word: words) {
            tempList.add(word);
        }
        int removeIndex = (int)(Math.random() * tempList.size());
        Word temp;
        int replacementSyllables = tempList.remove(removeIndex).getSyllables();
        while(replacementSyllables > 0) {
            int tempNum = (int)(Math.random() * wordBank.size());
            temp = wordBank.get(tempNum);
            if(replacementSyllables - temp.getSyllables() >= 0) {
                tempList.add(removeIndex++, temp);
                replacementSyllables -= temp.getSyllables();
            }
        }
        return new Phrase(tempList, wordBank);
        
        
//        ArrayList<Word> tempList = new ArrayList<>();
//        Random generator = new Random();
//        for(Word word: words) {
//            tempList.add(word);
//        }
//        int replacementSyllables = 0;
//        int mutateNumber = generator.nextInt(words.size() + 1);
//        ArrayList<Integer> removeIndices = new ArrayList<>();
////        int[] removeIndices = new int[mutateNumber];
//        for(int i = 0; i < mutateNumber; i++) {
//            removeIndices.add(generator.nextInt(words.size()));
//            replacementSyllables += tempList.remove(removeIndices.get(i).intValue()).getSyllables();
//        }
//        Word temp;
//        while(replacementSyllables > 0) {
//            temp = wordBank.get(generator.nextInt(wordBank.size()));
//            if(replacementSyllables - temp.getSyllables() >= 0) {
//                tempList.add(removeIndices.get(generator.nextInt(removeIndices.size())), temp);
//                replacementSyllables -= temp.getSyllables();
//            }
//        }
//        return new Phrase(tempList, wordBank);
    }
    
    @Override
    public String toString() {
        // Returns the phrase in string format
        String temp = "";
        for(Word a : words) {
            temp += a.getText() + " ";
        }
        return temp.substring(0, temp.length() - 1);
    }

    @Override
    public Iterator<Word> iterator() {
        // Overriding the iterator method to make the class iterable
        return new PhraseIterator();
    }
    
    public class PhraseIterator implements Iterator<Word> {
        // Basic iterator that navigates through the data member words so that
        // an iterator for loop can be used on the Phrase class
        int index = 0;
        
        @Override
        public boolean hasNext() {
            // Checks if index is out of the words's range
            return index < words.size();
        }
        
        @Override public Word next() {
            if(hasNext()) {
                return words.get(index++);
            }
            else {
                return null; // There is no next
            }
        }
    }
}